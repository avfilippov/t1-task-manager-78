package ru.t1.avfilippov.tm.api.service;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.avfilippov.tm.entity.dto.ProjectDto;

import java.util.List;

public interface ProjectDTOService {

    List<ProjectDto> findAll(String userId);

    ProjectDto save(@RequestBody ProjectDto project);

    ProjectDto save(String userId);

    ProjectDto findById(@PathVariable("id") String id);

    ProjectDto findByUserIdAndId(String userId, String id);

    boolean existsById(@PathVariable("id") String id);

    boolean existsByUserIdAndId(String userId, String id);

    long countByUserId(String userId);

    void deleteById(@PathVariable("id") String id);

    void deleteByUserIdAndId(String userId, String id);

    void delete(@RequestBody ProjectDto project);

    void deleteAll(@RequestBody List<ProjectDto> projects);

    void clear(String userId);

}
