package ru.t1.avfilippov.tm.config;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.t1.avfilippov.tm.api.endpoint.ProjectEndpoint;
import ru.t1.avfilippov.tm.api.endpoint.TaskEndpoint;

import javax.xml.ws.Endpoint;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebApplicationConfiguration extends WebSecurityConfigurerAdapter {//implements WebMvcConfigurer, WebApplicationInitializer {

    @Autowired
    private Bus bus;

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/services/*").permitAll()
                .antMatchers("/swagger-ui.html#").permitAll()
                .antMatchers("/api/auth/login").permitAll()
                .and()
                .authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .and()
                .logout().permitAll()
                .logoutSuccessUrl("/login")
                .and()
                .csrf().disable();
    }

    @Bean
    public Endpoint projectEndpointRegistry(final ProjectEndpoint projectEndpoint, Bus bus) {
        final EndpointImpl endpoint = new EndpointImpl(bus, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry(final TaskEndpoint taskEndpoint, Bus bus) {
        final EndpointImpl endpoint = new EndpointImpl(bus, taskEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

}
