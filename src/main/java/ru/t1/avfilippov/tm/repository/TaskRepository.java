package ru.t1.avfilippov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.avfilippov.tm.entity.model.Task;

public interface TaskRepository extends JpaRepository<Task, String> {
}
