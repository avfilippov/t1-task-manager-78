package ru.t1.avfilippov.tm.unit.repository;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import ru.t1.avfilippov.tm.entity.dto.ProjectDto;
import ru.t1.avfilippov.tm.marker.UnitCategory;
import ru.t1.avfilippov.tm.repository.ProjectDtoRepository;
import ru.t1.avfilippov.tm.util.UserUtil;

import javax.transaction.Transactional;

@Transactional
@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(UnitCategory.class)
public class ProjectDtoRepositoryTest {

    private final ProjectDto project1 = new ProjectDto("Test Project 1");

    private final ProjectDto project2 = new ProjectDto("Test Project 2");

    private final ProjectDto project3 = new ProjectDto("Test Project 3");

    private final ProjectDto project4 = new ProjectDto("Test Project 4");

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private ProjectDtoRepository projectRepository;

    @Before
    public void initTest() {
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        project1.setUserId(UserUtil.getUserId());
        project2.setUserId(UserUtil.getUserId());
        project3.setUserId(UserUtil.getUserId());
        project4.setUserId(UserUtil.getUserId());
        projectRepository.save(project1);
        projectRepository.save(project2);
        projectRepository.save(project3);
    }

    @After
    public void afterTest() {
        projectRepository.deleteAll();
    }

    @Autowired
    public void clean() {
        projectRepository.deleteAll();
    }

    @Test
    @Category(UnitCategory.class)
    public void deleteByUserIdTest() {
        projectRepository.deleteByUserIdAndId(UserUtil.getUserId(), project2.getId());
        Assert.assertNull(projectRepository.findByUserIdAndId(UserUtil.getUserId(), project2.getId()).orElse(null));
    }

    @Test
    @Category(UnitCategory.class)
    public void findByUserIdAndIdTest() {
        Assert.assertNotNull(projectRepository.findByUserIdAndId(UserUtil.getUserId(), project1.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void existByUserIdAndIdTest() {
        Assert.assertTrue(projectRepository.existsById(project3.getId()));
    }

}
