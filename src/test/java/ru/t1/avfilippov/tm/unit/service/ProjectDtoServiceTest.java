package ru.t1.avfilippov.tm.unit.service;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.avfilippov.tm.api.service.ProjectDTOService;
import ru.t1.avfilippov.tm.entity.dto.ProjectDto;
import ru.t1.avfilippov.tm.marker.UnitCategory;
import ru.t1.avfilippov.tm.util.UserUtil;

@Transactional
@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(UnitCategory.class)
public class ProjectDtoServiceTest {

    private final ProjectDto project1 = new ProjectDto("Test Project 1");

    private final ProjectDto project2 = new ProjectDto("Test Project 2");

    private final ProjectDto project3 = new ProjectDto("Test Project 3");

    private final ProjectDto project4 = new ProjectDto("Test Project 4");

    @Autowired
    private ProjectDTOService projectDtoService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        project1.setUserId(UserUtil.getUserId());
        project2.setUserId(UserUtil.getUserId());
        projectDtoService.save(project1);
        projectDtoService.save(project2);
    }

    @After
    public void afterTest() {
        projectDtoService.clear(UserUtil.getUserId());
    }

    @Test
    @Category(UnitCategory.class)
    public void findByUserIdAndIdTest() {
        Assert.assertNotNull(projectDtoService.findByUserIdAndId(UserUtil.getUserId(), project1.getId()));
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllTest() {
        Assert.assertEquals(2, projectDtoService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @Category(UnitCategory.class)
    public void remove() {
        projectDtoService.deleteById(project1.getId());
        Assert.assertNull(projectDtoService.findById(project1.getId()));
    }

}
